package main

import (
	"github.com/urfave/cli/v2"
)

// Edit 用于编辑指定的 SSH 配置文件。
// 如果没有提供配置名称，它会提示用户从菜单中选择一个配置。
// 然后，使用默认文本编辑器打开所选的配置文件。
func Edit(cCtx *cli.Context) error {
	configName := cCtx.Args().First()
	if configName == "" {
		configName = SelectMenu("Select Config to Edit", GetSSHConfigs(false), false)
	}
	OpenWithDefaultTextEditor(GetSSHDir(true) + "/" + configName)
	return nil
}
