package main

import (
	"fmt"

	"github.com/urfave/cli/v2"
)

// Print 打印指定配置名称的 SSH 配置。
// 如果没有提供配置名称，它会提示用户从菜单中选择一个配置。
func Print(cCtx *cli.Context) error {
	configName := cCtx.Args().First()
	if configName == "" {
		configName = SelectMenu("Select Config to Print", GetSSHConfigs(false), false)
	}
	config := GetSSHConfig(configName)
	fmt.Println(config)
	return nil
}
