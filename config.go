package main

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

// Config 包含 SSH 配置的所有信息。
type Config struct {
	Name     string `validate:"required"`
	HostName string `validate:"required,fqdn|ip"`
	Port     int    `validate:"required,min=1,max=65535"`
	User     string `validate:"required"`
	Category string `validate:"required"`
	SshKey   string `validate:"omitempty,filepath"`
}

// Validate 检查 Config 结构体的有效性。
// 它使用验证器执行验证，并在任何验证失败时返回错误。
func (c *Config) Validate() error {
	validate := validator.New(validator.WithRequiredStructEnabled())
	return validate.Struct(c)
}

// Save 将配置保存到文件并创建到它的符号链接。
// 如果 useKey 为 true，则配置包括 SSH 密钥文件路径。
// 如果 dryRun 为 true，则配置将打印到控制台而不是保存。
func (c *Config) Save(useKey bool, dryRun bool) {
	var config string
	if useKey {
		config = fmt.Sprintf("Host %v\n  HostName %v\n  IdentityFile %v\n  User %v\n  Port %v\n", c.Name, c.HostName, c.SshKey, c.User, c.Port)
	} else {
		config = fmt.Sprintf("Host %v\n  HostName %v\n  User %v\n  Port %v\n", c.Name, c.HostName, c.User, c.Port)
	}
	if dryRun {
		fmt.Println("Dry run: \n", config)
		return
	}
	CreateConfig(c.Name, config)
	LinkConfig(c.Name, c.Category)
	fmt.Printf("Config saved successfully! You can now connect to the host using 'ssh %s'.\n", c.Name)
}
