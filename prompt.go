package main

import (
	"fmt"
	"strings"

	"github.com/manifoldco/promptui"
	"golang.org/x/exp/slices"
)

// BasicPrompt 使用给定的提示信息提示用户输入，并将用户的输入作为字符串返回。
// 如果 allowEmpty 设置为 true，则允许空输入；否则，将持续提示用户直到提供非空输入。
func BasicPrompt(prompt string, allowEmpty bool) string {
	var input string
	for {
		fmt.Print(prompt)
		fmt.Scanln(&input)
		// 如果输入不为空或允许空输入，则跳出循环
		if input != "" || allowEmpty {
			break
		}
	}
	return input
}

// CheckExist 检查给定的名称是否存在于 SSH 配置中。
// 如果名称存在，则返回 true；否则返回 false。
func CheckExist(name string) bool {
	return slices.Contains(GetSSHConfigs(false), name)
}

// AskYesNo 使用问题提示用户，并期望得到是或否的回答。
// 如果用户提供空输入，则函数使用 defaultValue。
// 如果用户回答 "yes" 或 "y"，则返回 true；否则返回 false。
func AskYesNo(prompt string, defaultValue string) bool {
	fmt.Print(prompt)
	var input string
	fmt.Scanln(&input)
	input = strings.ToLower(input)
	if input == "" {
		input = defaultValue
	}
	return input == "y" || input == "yes"
}

// SelectMenu 显示一个带有项目列表的提示，并允许用户选择其中一个。
// 它接受一个提示文本、一个项目列表，以及一个标志，指示是否包含“添加新项”选项。
// 它返回选中的项目作为字符串。
func SelectMenu(promptText string, items []string, addNew bool) string {
	options := items
	if addNew {
		options = append(options, "Add New")
	}
	prompt := promptui.Select{
		Label:        promptText,
		Items:        options,
		HideSelected: true,
	}
	_, result, err := prompt.Run()
	if err != nil {
		fmt.Println("Something went wrong")
	}
	return result
}
