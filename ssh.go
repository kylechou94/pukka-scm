package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
)

// IGNORE_KEYS 是用于匹配应当被忽略的文件名的正则表达式。
// 在处理 SSH 相关操作时用于过滤掉某些文件名。
var IGNORE_KEYS = regexp.MustCompile(`(.old|.pub|.ppk|known_hosts|.dccache|.bak|config)`)

// categoryRegex 是用于匹配文件中分类行的正则表达式。
var categoryRegex = regexp.MustCompile(`(?m)^#.+$`)

// GetSSHDir 返回 SSH 目录的路径。
// 如果 `configs` 参数为真，它会在目录路径后追加 "/config.d"。
func GetSSHDir(configs bool) string {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Println(err)
		log.Fatal("Unable to get user home directory")
	}
	dir := home + "/.ssh"
	CreateDirIfNotExist(dir, 0644)
	if configs {
		dir += "/config.d"
		CreateDirIfNotExist(dir, 0644)
	}
	return dir
}

// GetSSHConfigs 返回 SSH 配置文件路径的列表。
//如果 `withBase` 为 true，则每个文件路径都会包含基目录路径。
func GetSSHConfigs(withBase bool) []string {
	dir := GetSSHDir(true)
	configs, err := os.ReadDir(dir)
	if err != nil {
		log.Println(err)
		log.Fatal("Unable to read ssh configs")
	}
	items := make([]string, 0, len(configs))
	for _, config := range configs {
		item := config.Name()
		if withBase {
			item = dir + "/" + item
		}
		items = append(items, item)
	}
	return items
}

// GetSSHKeys 返回 SSH 目录中找到的 SSH 密钥列表。
// 它读取 SSH 目录中的文件，并过滤掉目录和与 IGNORE_KEYS 模式匹配的密钥。
func GetSSHKeys() []string {
	var keys []string
	files, err := os.ReadDir(GetSSHDir(false))
	if err != nil {
		return keys
	}
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		if !IGNORE_KEYS.MatchString(file.Name()) {
			keys = append(keys, file.Name())
		}
	}
	return keys
}

// GetCategories 返回表示在 SSH 配置文件中找到的分类的字符串切片。
func GetCategories() []string {
	return categoryRegex.FindAllString(ReadSSHConfig(GetSSHDir(false)), -1)
}

// ReadSSHConfig 读取位于指定路径的 SSH 配置文件的内容。
// 它返回文件的内容作为字符串。
func ReadSSHConfig(path string) string {
	CheckIfExists(path)
	// CreateFileIfNotExist(path + "/config")
	file, err := os.Open(path + "/config")
	if err != nil {
		log.Fatal("Unable to open config file")
	}
	defer file.Close()

	buffer := make([]byte, 1024)
	var data string
	for {
		n, err := file.Read(buffer)
		if err != nil {
			break // End of file reached or an error occurred
		}
		data = string(buffer[:n])
	}
	return data
}

// CheckIfExists 检查指定的基路径是否包含必要的配置文件和目录。
// 如果配置文件或 config.d 目录不存在，则创建它们。
func CheckIfExists(basepath string) {
	if _, err := os.Stat(basepath + "/config"); os.IsNotExist(err) {
		// log.Fatal("CONFIG FILE DOES NOT EXIST")
		fmt.Println("Config file does not exist. Creating a new one.")
		file, err := os.Create(basepath + "/config")
		if err != nil {
			log.Fatal("Unable to create config file: ", err)
			return
		}
		defer file.Close()
	}

	if _, err := os.Stat(basepath + "/config.d"); os.IsNotExist(err) {
		log.Fatal("CONFIG.D DIRECTORY DOES NOT EXIST")
		err = os.Mkdir(basepath+"/config.d", 0755)
		if err != nil {
			log.Fatal("Unable to create config.d directory: ", err)
			return
		}
	}
}
// CreateConfig 使用给定的名称和内容创建一个新的 SSH 配置文件。
// 该文件在从 GetSSHDir(true) 获取的 SSH 目录中创建。
// 如果文件已存在，则会被覆盖。
func CreateConfig(name, config string) {
	file, err := os.Create(GetSSHDir(true) + "/" + name)
	if err != nil {
		log.Fatal("Unable to create config file: ")
	}
	defer file.Close()
	file.Write([]byte(config))
}

// LinkConfig 将配置文件链接到 SSH 配置文件。
// 它接受配置文件的名称和分类作为参数。
// 该函数打开 SSH 配置文件，读取其内容，并搜索匹配的分类。
// 如果找到匹配的分类，则将新配置文件附加到现有分类块中。
// 如果没有找到匹配的分类，则创建一个新的分类块并附加新配置文件。
// 最后，它将修改后的内容写回 SSH 配置文件。
func LinkConfig(name, category string) {
	file, err := os.OpenFile(GetSSHFile(), os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	content, err := io.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	pattern := regexp.MustCompile(`(?m)(?s)` + category + `\s.*?(?:^\B|\z)`)
	match := pattern.Find(content)
	if len(match) != 0 {
		var newBlock string
		if strings.HasSuffix(string(match), "\n") {
			newBlock = string(match) + "Include config.d/" + name + "\n"
		} else {
			newBlock = string(match) + "\nInclude config.d/" + name + "\n"
		}
		content = []byte(strings.Replace(string(content), string(match), newBlock, 1))
	} else {
		contentStr := string(content)
		lines := strings.Split(contentStr, "\n")
		lastLine := lines[len(lines)-1]
		if strings.TrimSpace(lastLine) == "" {
			content = append(content, []byte(fmt.Sprintf("\n# %v\nInclude config.d/%v\n", strings.ToUpper(category), name))...)
		} else {
			content = append(content, []byte(fmt.Sprintf("\n\n# %v\nInclude config.d/%v\n", strings.ToUpper(category), name))...)
		}
	}
	file.WriteAt(content, 0)
}

// GetSSHFile 返回 SSH 配置文件的路径。
func GetSSHFile() string {
	return GetSSHDir(false) + "/config"
}

// GetSSHConfig 检索具有指定名称的 SSH 配置文件的内容。
// 它打开位于 SSH 目录中的配置文件，并将其内容读入字符串。
// 如果无法打开文件，则记录致命错误并返回空字符串。
func GetSSHConfig(name string) string {
	file, err := os.Open(GetSSHDir(true) + "/" + name)
	if err != nil {
		log.Fatal("Unable to open config file")
	}
	defer file.Close()
	buffer := make([]byte, 1024)
	var data string
	for {
		n, err := file.Read(buffer)
		if err != nil {
			break // End of file reached or an error occurred
		}
		data = string(buffer[:n])
	}
	return data
}
