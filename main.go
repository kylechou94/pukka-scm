package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:    "pukka-scm",
		Usage:   "pukka ssh config manager",
		Version: "1.1.0",
		Authors: []*cli.Author{
			{
				Name:  "Kyle Chou",
				Email: "kylechou@devopsthink.org",
			},
		},
		Action: func(c *cli.Context) error {
			// 如果没有提供命令，则进入交互模式，提供用户选择 list、add、print 或 edit 命令
			if err := Interactive(); err != nil {
				log.Fatal(err)
			}
			return nil
		},
		Commands: []*cli.Command{
			// list command，带有 filter 标志
			{
				Name:    "list",
				Aliases: []string{"ls"},
				Flags: []cli.Flag{
					// filter 标志用于过滤 ssh 配置
					&cli.StringFlag{
						Name:    "filter",
						Aliases: []string{"f"},
						Usage:   "filter ssh configs",
					},
				},
				Usage:  "list available ssh configs",
				Action: List,
			},

			// add command 带有 dry-run 标志
			{
				Name:   "add",
				Usage:  "add new ssh config",
				Action: Add,
				Flags: []cli.Flag{
					// dry-run 标志用于打印配置到终端而不是保存它
					&cli.BoolFlag{
						Name:    "dry-run",
						Aliases: []string{"d"},
						Usage:   "dry run",
					},
				},
			},

			// print command 用于打印 ssh 配置
			{
				Name:      "print",
				Usage:     "print ssh config",
				Action:    Print,
				ArgsUsage: " <config name>",
			},

			// edit command 用于编辑 ssh 配置
			{
				Name:      "edit",
				Aliases:   []string{"e"},
				Usage:     "edit ssh config",
				Action:    Edit,
				ArgsUsage: " <config name>",
			},
		},
	}

	// 打印 Pukka 的 ASCII 艺术字
	PrintPukka()

	// 运行应用
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func Interactive() error {
	fmt.Println("Welcome to Pukka SSH Config Manager!")
	fmt.Println("Please select a command:")
	fmt.Println("1. list")
	fmt.Println("2. add")
	fmt.Println("3. print")
	fmt.Println("4. edit")
	fmt.Println("5. exit")
	fmt.Print("Enter the number of the command: ")

	// 读取用户输入
	var cmd string
	_, err := fmt.Scanf("%s", &cmd)
	if err != nil {
		return err
	}
	// todo: 根据用户输入执行相应的命令
	switch cmd {
	case "list":
		os.Exit(0)
	case "add":
		os.Exit(0)
	case "print":
		os.Exit(0)
	case "edit":
		os.Exit(0)
	case "exit":
		os.Exit(0)
	default:
		fmt.Println("Invalid command")
	}

	return nil
}
