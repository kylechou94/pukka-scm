# 用户操作手册

## 1. 系统概述

### 1.1 运行环境

`pukka-scm` 是一个专为公司内部开发的命令行工具软件，用于管理项目上所有服务器的 SSH 连接配置，支持以下操作系统：

- Windows
- macOS
- Linux

### 1.2 功能及性能

`pukka-scm` 提供以下主要功能，旨在简化和加速 SSH 连接配置的创建、编辑和管理过程：

- **列出现有的 SSH 连接配置**：快速查看所有已配置的 SSH 连接。
- **添加新的 SSH 连接配置**：轻松添加新的 SSH 连接配置。
- **打印指定的 SSH 连接配置**：查看特定 SSH 连接配置的详细信息。
- **编辑现有的 SSH 连接配置**：直接在命令行中编辑 SSH 连接配置。

### 1.3 其他

- **当前版本**：v1.0.1
- **作者**：周伟军
- **联系方式**：<zhouwj@pukkasoft.cn>

## 2. 使用须知

### 2.1 编写目的

本操作手册旨在帮助公司运维人员、系统管理员和开发人员全面了解 `pukka-scm` 命令行工具的安装、配置和使用方法，以确保用户能够高效、安全地进行 SSH 连接配置管理。

### 2.2 范围

本手册适用于所有希望使用 `pukka-scm` 管理 SSH 连接配置的用户，适用范围包括 Windows、macOS 和 Linux 系统。

### 2.3 定义和缩写词

- **SSH**：Secure Shell Protocol，安全外壳协议
- **CLI**：Command Line Interface，命令行界面
- **SCM**：SSH Config Manager，SSH 连接配置管理器
- **PUKKA**：帕科视讯科技（杭州）股份有限公司
- **Go**：Go 语言，一种开源编程语言

### 2.4 软件安装说明

#### 2.4.1 准备工作

1. 建议使用 **Linux 系统**，推荐发行版为 **openEuler 22.03 LTS SP4**，并确保能够访问互联网。
2. 确保已安装 **Go 语言环境**，建议使用 **1.22.5** 版本。
3. 下载最新版本的 **`pukka-scm` 源代码**，版本 **1.0.1**。
4. 确保已安装 **`vim`** 文本编辑器。

#### 2.4.2 编译安装

1. 解压源代码包并进入源代码目录：

```sh
tar -zxvf pukka-scm-v1.0.1.tar.gz
cd pukka-scm-v1.0.1
```

2. 下载依赖包：

```sh
go mod download
```

3. 编译源代码：

```sh
GOOS=linux GOARCH=amd64 go build -o pukka-scm
```

> **注意**：如果需要在其他系统上编译，请修改 `GOOS` 和 `GOARCH` 参数。

4. 将生成的可执行文件放置在系统的 `PATH` 路径中：

```sh
sudo mv pukka-scm /usr/local/bin
```

### 2.5 软件启动说明

#### 2.5.1 启动命令

**说明**：
运行以下命令启动 `pukka-scm`：

```sh
pukka-scm
```

#### 2.5.2 帮助命令

**说明**：
运行以下命令获取 `pukka-scm` 的帮助信息：

```sh
pukka-scm --help
```

#### 2.5.3 版本命令

**说明**：
运行以下命令查看 `pukka-scm` 的版本信息：

```sh
pukka-scm --version
```

#### 2.5.4 退出命令

**说明**：
运行以下命令退出 `pukka-scm`：

```sh
exit
```

## 3. 操作说明

### 3.1 列出配置 (`list` 命令)

列出当前所有可用的 SSH 连接配置。

**命令格式：**
列出所有可用的 SSH 连接配置，支持使用 `--filter` 选项进行过滤。

```sh
pukka-scm list [--filter <过滤条件>]
```

**示例：**
添加一个新的 SSH 连接配置，`--dry-run` 选项允许用户预览将要添加的配置，而不实际保存。

```sh
pukka-scm list --filter "example"
```

### 3.2 添加配置 (`add` 命令)

添加一个新的 SSH 连接配置。

**命令格式：**
查看指定的 SSH 连接配置的详细信息。如果未指定配置名称，则使用交互式菜单选择配置。

```sh
pukka-scm add [--dry-run]
```

- **`--dry-run`**：预览添加的配置，不会真正写入配置文件。

**示例：**

```sh
pukka-scm add --dry-run
pukka-scm add
```

### 3.3 打印配置 (`print` 命令)

查看指定的 SSH 连接配置的详细信息。

**命令格式：**
查看指定的 SSH 连接配置的详细信息。如果未指定配置名称，则使用交互式菜单选择配置。

```sh
pukka-scm print <配置名称>
```

**示例：**

```sh
pukka-scm print my-config
pukka-scm print
```

### 3.4 编辑配置 (`edit` 命令)

直接在命令行中编辑指定的 SSH 连接配置。

**命令格式：**
直接在命令行中编辑指定的 SSH 连接配置。如果未指定配置名称，则使用交互式菜单选择配置。

```sh
pukka-scm edit <配置名称>
```

**示例：**

```sh
pukka-scm edit my-config
pukka-scm edit
```

## 4. 命令详解

本节提供 `pukka-scm` 命令的详细说明，帮助用户更好地理解每个命令的用途和使用方法。

### 4.1 `list` 命令

列出所有可用的 SSH 连接配置，支持使用 --filter 选项进行过滤，以便快速找到所需配置。该命令无参数时，列出所有配置。

### 4.2 `add` 命令

添加一个新的 SSH 连接配置，`--dry-run` 选项允许用户预览将要添加的配置，而不实际保存，以便于检查配置信息的准确性。

### 4.3 `print` 命令

打印指定的 SSH 连接配置详细信息，便于用户查看和验证配置的具体内容。如果未指定配置名称，则使用交互式菜单选择配置。

### 4.4 `edit` 命令

使用默认文本编辑器编辑指定的 SSH 连接配置，方便用户快速修改配置信息。如果未指定配置名称，则使用交互式菜单选择配置。

## 5. 故障排除

### 5.1 常见问题

- **无法读取 SSH 连接配置文件**：确保 `${HOME}/.ssh` 目录、`${HOME}/.ssh/config` 文件和 `${HOME}/.ssh/config.d` 目录存在并具有正确的权限设置。
- **添加配置失败**：检查配置名称是否重复，确保所有必填信息已正确填写。

### 5.2 日志与报错

所有操作日志和错误信息将输出到控制台。如遇问题，请首先检查命令行输出的错误信息，以便快速定位和解决问题。

## 6. 附录

### 6.1 配置文件结构

SSH 连接配置文件位于 `~/.ssh/config` 和 `~/.ssh/config.d` 目录下，格式如下：

```sh
Host <配置名称>
  HostName <主机名>
  User <用户名>
  Port <端口号>
  IdentityFile <SSH密钥路径> # 可选
```

### 6.2 参考链接

- [Go 语言文档](https://golang.org/doc/)
- [SSH 连接配置文档](https://www.ssh.com/academy/ssh/config)
- [Linux 命令行教程](https://www.linuxcommand.org/tlcl.php)
- [Vim 文本编辑器教程](https://www.vim.org/docs.php)
- [openEuler 官方文档](https://docs.openeuler.org/zh/)
