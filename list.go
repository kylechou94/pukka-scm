package main

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"
)

// List 根据提供的过滤器打印 SSH 配置的名称。
// 如果没有提供过滤器，则打印所有 SSH 配置。
func List(cCtx *cli.Context) error {
	var output string
	for _, name := range GetSSHConfigs(false) {
		if cCtx.String("filter") != "" {
			if strings.Contains(name, cCtx.String("filter")) {
				output += fmt.Sprintf("%v\n", name)
			}
		} else {
			output += fmt.Sprintf("%v\n", name)
		}
	}
	fmt.Print(output)
	return nil
}
