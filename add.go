package main

import (
	"fmt"

	"github.com/urfave/cli/v2"
)

// Add 是一个基于用户输入添加新配置的函数。
// 它提示用户输入各种配置详情，如名称、主机名、端口、用户名、SSH密钥和类别。
// 该函数验证配置并保存它。
// 如果配置名称已存在，将提示用户选择另一个名称。
// 如果配置验证或保存出现问题，函数返回错误。
func Add(cCtx *cli.Context) error {
	// Prompt the user to enter configuration details
	var name string
	for {
		name = BasicPrompt("Name your config: ", false)
		if !CheckExist(name) {
			break
		}
		fmt.Println("The config name already exists. Please choose another name.")
	}

	var config Config
	config.Name = name
	config.HostName = BasicPrompt("Enter the hostname: ", false)
	port := BasicPrompt("Enter the port (Default: 22): ", true)
	if port != "" {
		config.Port = ParseInt(port)
	}
	config.Port = 22
	config.User = BasicPrompt("Enter the username: ", false)
	// Add ssh key
	useKey := AskYesNo("Use ssh key? (Y/n): ", "y")
	if useKey {
		result := SelectMenu("Select SSH Key", GetSSHKeys(), true)
		if result == "Add New" {
			config.SshKey = BasicPrompt("Enter the ssh key full path: ", false)
		} else {
			config.SshKey = "~/.ssh/" + result
		}
	}
	// Add category
	result := SelectMenu("Select Category", GetCategories(), true)
	if result == "Add New" {
		config.Category = BasicPrompt("Enter New Category (without #): ", false)
	} else {
		config.Category = result
	}
	// Validate the config
	err := config.Validate()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	config.Save(useKey, cCtx.Bool("dry-run"))
	return nil
}
