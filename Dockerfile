# Description: Dockerfile for building the image for the SCM and use for the test
# Stage 1: Build the binary
FROM golang:1.22.5 AS builder

WORKDIR /scm

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o scm .

# Stage 2: Use the binary for test on docker
FROM debian:bookworm-slim

WORKDIR /scm

COPY --from=builder /scm/scm .

COPY id_ed25519 id_ed25519.pub ./

RUN set -eux \
    && mkdir /root/.ssh \
    && mv id_ed25519 id_ed25519.pub /root/.ssh/
