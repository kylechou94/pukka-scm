# pukka-scm - PUKKA SSH Config Manager - PUKKA SSH 配置管理器

pukka-scm 是一个基于命令行的 SSH 配置管理工具，旨在帮助用户高效管理其 SSH 配置。通过简单的命令，用户可以添加、编辑、列出和打印 SSH 配置，从而提高工作效率。

## 主要特性

- **列出 SSH 配置**：支持通过过滤条件列出 SSH 配置。
- **添加新的 SSH 配置**：允许用户添加新的 SSH 配置，并支持预览（dry-run）功能。
- **打印 SSH 配置详情**：可以打印指定 SSH 配置的详细信息。
- **编辑 SSH 配置**：支持编辑指定的 SSH 配置。

## 安装指南

要使用 pukka-scm，您需要先在您的机器上安装 Go 语言环境。安装完成后，按照以下步骤操作：

1. 克隆仓库到本地：

```bash
git clone https://github.com/your-repo/pukka-scm.git
```

2. 进入项目目录：

```bash
cd pukka-scm
```

3. 编译项目：

```bash
GOOS=linux GOARCH=amd64 go build -o pukka-scm
```

## 使用说明

### 列出 SSH 配置

```bash
./pukka-scm list --filter "过滤条件"
```

### 添加新的 SSH 配置

```bash
./pukka-scm add --dry-run
```

### 打印 SSH 配置详情

```bash
./pukka-scm print <配置名称>
```

### 编辑 SSH 配置

```bash
./pukka-scm edit <配置名称>
```
