package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strconv"
)

// ParseInt 将给定的字符串解析为对应的整数值。
// 如果字符串无法解析为整数，函数将记录错误并退出程序。
func ParseInt(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

// OpenWithDefaultTextEditor 使用操作系统的默认文本编辑器打开指定文件。
// 支持不同的操作系统：macOS、Windows 和 Linux。
// 如果操作系统不支持或执行命令时出错，函数将返回错误。
func OpenWithDefaultTextEditor(filename string) error {
	var cmd *exec.Cmd

	switch runtime.GOOS {
	case "darwin":
		cmd = exec.Command("open", filename)
	case "windows":
		cmd = exec.Command("cmd", "/c", "start", filename)
	case "linux":
		cmd = exec.Command("vim", filename)
	default:
		return fmt.Errorf("unsupported operating system: %s", runtime.GOOS)
	}

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

// CreateDirIfNotExist 如果目录不存在，则创建目录。
// 接受目录路径作为第一个参数，文件权限模式作为第二个参数。
// 如果目录已存在，则不执行任何操作。
// 创建目录时出现错误，则返回错误并退出程序。
// 如果创建目录成功，则返回 nil。
func CreateDirIfNotExist(dir string, perm os.FileMode) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.Mkdir(dir, perm)
		if err != nil {
			log.Fatalf("Unable to create directory %s: %v", dir, err)
		}
	}
	return nil
}

// CreateFileIfNotExist 如果文件不存在，则创建文件。
// 如果文件已经存在，则此函数不执行任何操作。
// 如果创建文件时出现问题，则返回错误并退出程序。
// 如果创建文件成功，则返回 nil。
func CreateFileIfNotExist(file string) error {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		_, err = os.Create(file)
		if err != nil {
			log.Fatalf("Unable to create file %s: %v", file, err)
		}
	}
	return nil
}

// PrintPukka 打印 Pukka 的 ASCII 艺术字。
func PrintPukka() {
	pukkaArt := `
    ██████╗ ██╗   ██╗██╗  ██╗██╗  ██╗ █████╗
    ██╔══██╗██║   ██║██║ ██╔╝██║ ██╔╝██╔══██╗
    ██████╔╝██║   ██║█████╔╝ █████╔╝ ███████║
    ██╔═══╝ ██║   ██║██╔═██╗ ██╔═██╗ ██╔══██║
    ██║     ╚██████╔╝██║  ██╗██║  ██╗██║  ██║
    ╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝
    `
	fmt.Println(pukkaArt)
}
